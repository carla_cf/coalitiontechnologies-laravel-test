<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class FormController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function send_form()
    {

        return view('form.form');

    }

    public function save_product(){

        $jsondata = json_encode($_POST['products'], JSON_PRETTY_PRINT);

        if(file_put_contents('products.json', $jsondata)) {

            $result = array(
                'status' => 'success'
            );

        } else {

            $result = array(
                'status' => 'fail'
            );
        }

        return response()->json($result);
    }

    function get_products() {

        if( $content = file_get_contents('products.json') ) {

            $products = json_decode($content);

            $result = array(
                'status' => 'success',
                'products' => $products
            );

        } else {

            $result = array(
                'status' => 'fail'
            );
        }

        return response()->json($result );

    }
}
