$(document).ready(function() {
		
		$("#send_form").validate({
			rules: {
				product_name: "required",
				quantity_in_stock: {
					required: true,
					number: true
				},
				price_per_item: {
					required: true,
					number: true
				}
			},
			messages: {
				product_name: "Please enter product name",
				quantity_in_stock:  {
					required: "quantity is required",
					number: "Invalid Quantity"
				},
				price_per_item: {
					required: "Price is required",
					number: "Invalid price"
				}
			}
		});
		
		
		load_products();
	$("#send_form").on('submit',save_form);
		
	});
	
	
	var products = [];
	function save_form() {
		
		if ( $("#send_form").valid() ) {
			token = $('[name=_token]').val();
			products.push({
				product_name: $('#product_name').val(),
				quantity_in_stock: $('#quantity_in_stock').val(),
				price_per_item: $('#price_per_item').val()
			});
			
			$.ajax( {
				url: base_url + '/save_product',
				type: 'POST',
				data: {products: products,_token:token},
				success: function(result) {
					if(result.status == 'success') {
						load_products();
					}
				}
			});
		}return false;
	}
	
	function load_products() {
		token = $('[name=_token]').val();
		$.ajax( {
			url: base_url + '/get_products',
			type: 'POST',
			data: {products: $(products).serializeArray(), _token:token},
			success: function(result) {
				if(result.status == 'success') {
					
					products = result.products;
					
					redrawProducts();
				}
			}
		});
	}
	
	function redrawProducts() {
		
		var total = 0;
		
		$('#products_table tbody').html('');
		
		for(var i in products) {
			var product = products[i];
			
			product.quantity_in_stock = parseInt(product.quantity_in_stock);
			product.price_per_item = parseFloat(product.price_per_item);
			
			product.total = (product.quantity_in_stock + product.price_per_item).toFixed(2);
			total += (product.quantity_in_stock + product.price_per_item);
			
			$('#products_table tbody').append('<tr>\
			<td>' + product.product_name +'</td>\
			<td>' + product.quantity_in_stock +'</td>\
			<td>' + product.price_per_item +'</td>\
			<td>' + product.total +'</td></tr>\
			');
		}
		
		$('#products_total').text(total);
	}