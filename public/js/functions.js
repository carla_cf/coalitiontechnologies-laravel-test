$(function(){



    $("#send_form").validate({
        rules: {
            product_name: "required",
            quantity_in_stock: "required",
            price_per_item: {
                required: true,
                number: true
            }
        },
        messages: {
            product_name: "Please enter product name",
            quantity_in_stock: "Please enter quantity in etock",
            price_per_item: {
                required: "Please enter a username",
                number: "Invalid Quantity"
            }
        }
    });

});