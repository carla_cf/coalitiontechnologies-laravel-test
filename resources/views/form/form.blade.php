@extends('layout.layout')

@section('content')






    {!! Form::open(['url' => 'ser', 'class' => 'form-horizontal', 'id'=>'send_form']) !!}

    <div class="form-group {{ $errors->has('product_name') ? 'has-error' : ''}}">
        {!! Form::label('product_name', 'Product name: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('product_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('product_name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('quantity_in_stock') ? 'has-error' : ''}}">
        {!! Form::label('quantity_in_stock', 'Quantity in stock: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('quantity_in_stock', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('quantity_in_stock', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('price_per_item') ? 'has-error' : ''}}">
        {!! Form::label('price_per_item', 'Price per item: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('price_per_item', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('price_per_item', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>




    {!! Form::close() !!}


<table id="products_table" class="table table-responsive">
<tbody></tbody><tfoot><tr><td></td><td></td><td></td><td ><div id="products_total"></div></td></tr></tfoot>
</table>



@endsection